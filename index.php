<?
session_start();

if(isset($_GET['logout'])){	
	
	//Simple exit message
	$fp = fopen("log.html", 'a');
	fwrite($fp, "<div class='msgln'><i>User ". $_SESSION['name'] ." has left the chat session.</i><br></div>");
	fclose($fp);
	
	session_destroy();
	header("Location: index.php"); //Redirect the user
}

function loginForm(){
	echo'
	<div id="loginform">
	<form action="index.php" method="post" class="loginform">
		<p>Please enter your name to continue:</p>
		<label for="name">Name:</label>
		<input type="text" name="name" id="name" autofocus="autofocus" />
		<label for="password">Password:</label>
		<input type="password" name="password" id="password" />
		<input type="submit" name="enter" id="enter" value="Enter" />
	</form>
	</div>
	';
}


if(isset($_POST['enter'])){

	// turns entered login name to lowercase
	$theName = strtolower($_POST['name']);

	// List of Login names and Passwords
	$login = array(
		'jansbury' => '1111',
		'molly' => '2222',
		'briar' => '3333',
		'carl' => '4444',
		'guest' => '0000'
	);

	// Array of login names
	$loginNames = array_keys($login);

	//if the entered login name equals a name in the list
	if(in_array($theName, $loginNames)){
		//if the password equals the login name password
		if ($_POST['password'] == $login[$theName]) {
			//set session name to entered login name
			$_SESSION['name'] = stripslashes(htmlspecialchars($_POST['name']));
		}
		else{
			echo '<span class="error">Please type in the correct password</span>';
		}
	}
	else{
		echo '<span class="error">Please type in a name</span>';
	}
}
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Pollen Chat</title>
<meta name="viewport" content="width=device-width">
<meta name="robots" content="nofollow, noindex" />
<link type="text/css" rel="stylesheet" href="style.css" />
</head>

<?php
if(!isset($_SESSION['name'])){
	loginForm();
}
else{
?>
<div id="wrapper">
	<div id="menu">
		<p class="welcome">login: <b><?php echo $_SESSION['name']; ?></b>&nbsp;<?php echo date("D M d  h:i:s"); ?></p>
		
		<p class="logout"><a id="exit" href="#">Exit Chat</a></p>
		<p class="control"><a id="controlls" href="#">Controls</a></p>
		<div style="clear:both"></div>
	</div>	
	<div id="chatbox"><?php
	if(file_exists("log.html") && filesize("log.html") > 0){
		$handle = fopen("log.html", "r");
		$contents = fread($handle, filesize("log.html"));
		fclose($handle);
		
		echo $contents;
	}
	?></div><!-- chatbox -->
	
	<form name="message" action="" class="chatform">
		<input name="usermsg" type="text" id="usermsg" size="63" autofocus="autofocus" /> 
		<input name="submitmsg" type="submit"  id="submitmsg" value="Send"  />
	</form>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="js/pollen.js"></script>

<?php
}
?>
</body>
</html>
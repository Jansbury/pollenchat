<?
session_start();
if(isset($_SESSION['name'])){
	$text = $_POST['text'];
	date_default_timezone_set(NZ);

	// IF MESSAGE EQUALS "Clear!" THEN CLEAR THE CONVERSATION
	if ($text == "Clear!") {
		$file=fopen("log.html","w");
		fwrite($file, "<div class='msgln time'>".date("g:i A")." - <span class='userName'> ".$_SESSION['name']."</span><span> has cleared the conversation</span></div>");
		fclose($file);
	}

	// IF MESSAGE EQUALS "Clear1" THEN CLEAR THE LAST MESSAGE
	elseif($text == "Clear1") {
		$handle = fopen("log.html", "r");
		$contents = fread($handle, filesize("log.html"));
		fclose($handle);

		$pattern = '/(<div class=\'msgln\'><div class=\'time\'>[^<]+<span class=\'userName\'>[^<]+<\/span><\/div>[^<]+<br><\/div>)$/';
		$replacement = "";
		$clear1 = preg_replace($pattern, $replacement, $contents);

		$file=fopen("log.html","w");
		fwrite($file, $clear1);
		fclose($file);

	// EMOTICONS
	}elseif(preg_match('/(:[\)|\(])/', $text)){
		$pattern = '/(:\))+/';
		$replacement = "<span class='smile' ></span>";
		$text = preg_replace($pattern, $replacement, $text);
		$pattern = '/(:\()+/';
		$replacement = "<span class='sad' ></span>";
		$text = preg_replace($pattern, $replacement, $text);
		$pattern = '/(:\|)+/';
		$replacement = "<span class='serious' ></span>";
		$text = preg_replace($pattern, $replacement, $text);
		$pattern = '/(:D)+/';
		$replacement = "<span class='happy' ></span>";
		$text = preg_replace($pattern, $replacement, $text);

		$fp = fopen("log.html", 'a');
		fwrite($fp, "<div class='msgln'><div class='time'>".date("g:i A")." - <span class='userName'> ".$_SESSION['name']."</span></div>".$text."<br></div>");
		fclose($fp);

	}else{
		// ELSE WRITE THE MESSAGE
		$fp = fopen("log.html", 'a');
		fwrite($fp, "<div class='msgln'><div class='time'>".date("g:i A")." - <span class='userName'> ".$_SESSION['name']."</span></div>".stripslashes(htmlspecialchars($text))."<br></div>");
		fclose($fp);
	}
}


 if(isset($_GET["ip"])){
   echo geolocate($_GET["ip"]);
 }

 function geolocate($ip){
   $raw_html = file_get_contents("http://www.geody.com/geoip.php?ip=$ip");
   if(preg_match('/Location:(.*)/',$raw_html,$matches)){
     $location_raw = $matches[1];

     //Get rid of pesky HTML tags
     $location = preg_replace("/<[^>]*>/","",$location_raw);
     return $location;
   }else{
     return "ERROR";
   }
 }
?>